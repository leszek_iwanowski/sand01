extends KinematicBody2D

#export var bullet_scene: PackedScene
export (int) var detect_radius
export (PackedScene) var Ziarnko
export (PackedScene) var Rock
export (float) var fire_rate
var vis_color = Color(.867, .91, .247, 0.1)
var laser_color = Color(1.0, .329, .298)
var checker = []
var direction
var zmiennatestowa = 1
var licznik = 1
var checker_index
var position_checker = Vector2(0,0)

#var target
var hit_pos
var can_shoot = true

var speed = 300

var score: int
var timer: float


func _ready():
	checker.resize(10000)
	for i in range(10000):
		checker[i] = "0"
	$ShootTimer.wait_time = fire_rate

func get_move():
	#sterowanie graczem
	var move: Vector2
	if (Input.is_key_pressed(KEY_D) || Input.is_key_pressed(KEY_RIGHT)):
		move.x += 1
	if (Input.is_key_pressed(KEY_A) || Input.is_key_pressed(KEY_LEFT)):
		move.x -= 1
	if (Input.is_key_pressed(KEY_S) || Input.is_key_pressed(KEY_DOWN)):
		move.y += 1
	if (Input.is_key_pressed(KEY_W) || Input.is_key_pressed(KEY_UP)):
		move.y -= 1
	return move

func get_action():
	var action: Vector2
	if (Input.is_key_pressed(KEY_E) || Input.is_mouse_button_pressed(BUTTON_LEFT)):
		action.x = 1
	if (Input.is_key_pressed(KEY_Q) || Input.is_mouse_button_pressed(BUTTON_RIGHT)):
		action.y = 1
	return action
	
func aim():
	# Strzelamy przed siebie więc kąt taki jaki ma gracz
	var pos = $Sprite.get_local_mouse_position()
	if can_shoot:
		shoot(pos)

func aim2():
	# Strzelamy przed siebie więc kąt taki jaki ma gracz
	var pos = $Sprite.get_local_mouse_position()
	set_the_rock(pos)

func shoot(pos):
	# sprawdzamy czy jest miejsce na nowe ziarnko
	checker_index = 100 * round(global_position.y / 4) + round(global_position.x / 4)
	position_checker = Vector2(4 * round(global_position.x / 4), 4 * round(global_position.y / 4))
	for j in range(5):
		for i in range(5):
			if checker[checker_index + i - 100 * j] == "0" && can_shoot:
				add_ziarenko(checker_index + i - 100 * j, Vector2(position_checker.x + i*4, position_checker.y - j*4))
				can_shoot = false
			elif checker[checker_index - i - 100 * j] == "0" && can_shoot:
				add_ziarenko(checker_index - i - 100 * j, Vector2(position_checker.x - i*4, position_checker.y - j*4))
				can_shoot = false
	
	can_shoot = false
	$ShootTimer.start()

func set_the_rock(pos):
	# sprawdzamy czy jest miejsce na nową skałę
	checker_index = 100 * round(global_position.y / 4) + round(global_position.x / 4)
	position_checker = Vector2(4 * round(global_position.x / 4), 4 * round(global_position.y / 4))
	for j in range(2):
		for i in range(2):
			if checker[checker_index + i - 100 * j] == "0" && can_shoot:
				add_rock(checker_index + i - 100 * j, Vector2(position_checker.x + i*4, position_checker.y - j*4))
			elif checker[checker_index - i - 100 * j] == "0" && can_shoot:
				add_rock(checker_index - i - 100 * j, Vector2(position_checker.x - i*4, position_checker.y - j*4))


func _physics_process(delta):
	
	for i in range (96, 0, -1):
		# i - wiersz, j- kolumna
		for j in range (4, 97):
			if checker [i*100 + j] != "0" && checker [i*100 + j] != "1":
				# sprawdzamy komórkę pniżej
				if checker [(i+1)*100 + j] == "0":
					# jeżeli pusta to przenosimy w dół
					checker [(i+1)*100 + j] = checker [i*100 + j] # kopiujemy do komórki poniżej
					get_parent().get_node(checker [(i+1)*100 + j]).position = Vector2(j*4,(i+1)*4) # ustawiamy nowe współrzędne
					checker [i*100 + j] = "0" # czyścimy górną komórkę
				elif checker [(i+1)*100 + j] != "0":
					randomize()
					direction = randi()%2
					if direction == 0:
						direction = -1
					if checker [(i+1)*100 + j + direction] == "0":
						#przesuwany do komórki poniżej po lewej lub prawej stronie
						#randomize gwarantuje losowość strony z której zaczynamy
						checker [(i+1)*100 + j + direction] = checker [i*100 + j] # kopiujemy do komórki poniżej
						get_parent().get_node(checker [(i+1)*100 + j + direction]).position = Vector2((j+direction)*4,(i+1)*4) # ustawiamy nowe współrzędne
						checker [i*100 + j] = "0" # czyścimy górną komórkę
					elif checker [(i+1)*100 + j - direction] == "0":
						checker [(i+1)*100 + j - direction] = checker [i*100 + j] # kopiujemy do komórki poniżej
						get_parent().get_node(checker [(i+1)*100 + j - direction]).position = Vector2((j-direction)*4,(i+1)*4) # ustawiamy nowe współrzędne
						checker [i*100 + j] = "0" # czyścimy górną komórkę
	
	
	var action = Vector2(0,0)
	action = get_action()
	if (action.x == 1):
		aim()
	elif (action.y == 1):
		aim2()
	
	$Sprite.rotation = get_local_mouse_position().angle()
	
	var move = Vector2(0,0)
	move = get_move()
	
	#move_and_slide(move.normalized() * speed)
	move_and_slide(move.rotated(get_local_mouse_position().angle() + PI/2) * speed)
	update()

func add_ziarenko(index, pos):
	var b = Ziarnko.instance()
	get_parent().add_child(b)
	b.set_owner(get_parent())
	b.start(pos, get_local_mouse_position().angle())
	checker[index] = b.name

func add_rock(index, pos):
	var b = Rock.instance()
	get_parent().add_child(b)
	b.set_owner(get_parent())
	b.start(pos, get_local_mouse_position().angle())
	checker[index] = "1"


func _on_ShootTimer_timeout():
	can_shoot = true
